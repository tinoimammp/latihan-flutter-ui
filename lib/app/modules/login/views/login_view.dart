import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Container(
            width: Get.width,
            child: Image.asset(
              "assets/images/bg.png",
              fit: BoxFit.cover,
            ),
          ),
          //Layer Body
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 135),
                Container(
                  width: 150,
                  height: 150,
                  // width: Get.width,
                  child: Image.asset(
                    "assets/images/pic-1.png",
                    fit: BoxFit.contain,
                  ),
                ),
                SizedBox(height: 40),
                Text(
                  "Welcome To",
                  style: TextStyle(
                    fontSize: 32,
                    color: Colors.black87,
                  ),
                ),
                Text(
                  "Dirbox Cloud",
                  style: TextStyle(
                    fontSize: 45,
                    fontWeight: FontWeight.bold,
                    color: Colors.blue,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  "Reference site about Lorem Ipsum, giving information on its origins, as well as a random Lipsum generator.",
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.blueGrey,
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  width: 250,
                  // color: Colors.amber,
                  child: Text(
                    "Free upload File its totally free!",
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.blueGrey,
                    ),
                  ),
                ),
                SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(
                      onPressed: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset("assets/icons/finger.png"),
                          SizedBox(width: 10),
                          Text(
                            "Scan",
                            style: TextStyle(
                              color: Color.fromARGB(255, 19, 145, 195),
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                      style: ElevatedButton.styleFrom(
                        primary:
                            Color.fromARGB(0, 198, 236, 249).withOpacity(0.9),
                        fixedSize: Size(155, 50),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(90),
                        ),
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Sign In",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(width: 10),
                          Image.asset("assets/icons/panah-kanan.png"),
                        ],
                      ),
                      style: ElevatedButton.styleFrom(
                        primary:
                            Color.fromARGB(0, 10, 192, 65).withOpacity(0.5),
                        fixedSize: Size(155, 50),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 25),
                Center(
                  child: Text(
                    "Masuk dengan",
                    style: TextStyle(
                      fontSize: 14,
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("assets/icons/ig.png"),
                    SizedBox(width: 60),
                    Image.asset("assets/icons/fb.png"),
                    SizedBox(width: 60),
                    Image.asset("assets/icons/twitter.png"),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Center(
                  child: Text("Buat Akun Baru"),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
